const main = process.cwd() + '/';
const httpStatus = require('http-status');
const logger = require(main + 'utils/logger');
const APIError = require(main + 'utils/APIError');
const db = require(main + 'api/models/index.model');
const mapper = require(main + 'api/mappers/framework.mapper');

const getFramework = async (req, res, next) => {
    try {
        let { id } = req.params;
        let currentFramework = await db.framework.findOne({ where: { id } });
        if(!currentFramework) return next(new APIError(`Not found framework with this ID`, httpStatus.NOT_FOUND));
        let mappedFramework = mapper.mapFrameworkResponse(currentFramework);
        return res.json({status: httpStatus.OK, framework: mappedFramework});
    } catch (error) {
        logger.error(`[FRAMEWORK CONTROLLER] (getFramework) error: ${error}`);
        return next(error);
    }
};

module.exports = {
    getFramework
}