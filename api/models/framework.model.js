module.exports = (sequelize, Sequelize) => {
    Framework = sequelize.define('framework', {
        name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        version: {
            type: Sequelize.STRING,
            allowNull: false
        }
    });
    return Framework;

};