const main = process.cwd() + '/';
const Sequelize = require('sequelize');
const config = require(main + 'config/config.json');

const mysqlConfig = config.db.mysql;
const Op = Sequelize.Op;

const db = {};
const sequelize = new Sequelize(mysqlConfig.database, mysqlConfig.username, mysqlConfig.password, {
    host: mysqlConfig.host,
    logging: false,
    dialect: 'mysql',
    operatorsAliases: Op,
    port: 3306,
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    },
});

const modelsPath = `${main}api/models/`;

const ModelName = sequelize.import(`${modelsPath}framework.model`);
db[ModelName.name] = ModelName;

Object.keys(db).forEach(function (modelName) {
    if ("associate" in db[modelName]) {
        db[modelName].associate(db);
    }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;