const main = process.cwd() + '/';
const logger = require(main + 'utils/logger');
const db = require(main + 'api/models/index.model');
const cronService = require(main + 'api/services/cron.service');

const startTasks = async () => {
    try {
        logger.info(`[STARTER SERVICE] Run!`);

        cronService.startCronService();
        await checkFramework();

    } catch (error) {
        logger.error(`[STARTER SERVICE] (startTasks) error: ${error}`);
    }
}

const checkFramework = async () => {
    try {
        let currentFramework = await db.framework.findOne({ where: { name: 'Universe' } });
        if (!currentFramework) {
            await db.framework.create({
                name: 'Universe',
                version: '0.0.1'
            });
        }
    } catch (error) {
        logger.error(`[STARTER SERVICE] (checkFramework) error: ${error}`);
    }
}

module.exports = {
    startTasks
}