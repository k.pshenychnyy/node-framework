const cron = require('node-cron');
const INTERVAL = '*/5 * * * *'; //At every 5th minute

const cronFunction = async () => {
    try {
        logger.info('[CRON SERVICE] 5 minutes left!:)')
    } catch (error) {
        logger.error(`[CRON SERVICE] (cronFunction) error: ${error}`);
        return;
    }
};

const startCronService = () => {
    const cronTask = cron.schedule(INTERVAL, cronFunction, false);
    cronTask.start();
}

module.exports = { 
    startCronService 
};