const express = require('express');
const router = express.Router();

const main = process.cwd() + '/';
const frameworkController = require(main + 'api/controllers/framework.controller');

router.get('/:id', frameworkController.getFramework);

module.exports = router;