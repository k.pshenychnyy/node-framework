const main = process.cwd() + '/';
const express = require('express');
const router = express.Router();

const frameworkRoutes = require(main + 'api/routes/framework.route');

router.use('/framework', frameworkRoutes);
//Add routes

module.exports = router;