module.exports = {
    mapFrameworkResponse(framework) {
        let { name, version } = framework
        return { name, version };
    }
}