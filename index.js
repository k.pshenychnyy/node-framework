const main = process.cwd() + '/';

const httpStatus = require('http-status');
const config = require(main + 'config/config.json');
const logger = require(main + 'utils/logger');
const app = require(main + 'app');
const http = require('http');
const APIError = require(main + 'utils/APIError');
const db = require(main + 'api/models/index.model');
const routes = require(main + 'api/routes/index.route');

const server = http.createServer(app);

app.use('/api', routes);

app.use((err, req, res, next) => {// if error is not an instanceOf APIError, convert it.
    let apiError = new APIError();
    if (err instanceof APIError)
        apiError = new APIError(err.message, err.status, err.isPublic);

    logger.warn("[ERROR][API SERVER] err " + err.message);
    return res.json({
        status: apiError.status || httpStatus.INTERNAL_SERVER_ERROR,
        error: apiError.isPublic ? apiError.message : "Internal server error"
    });
});

app.use((req, res, next) => {// catch 404 and forward to error handler
    return res.json({
        status: httpStatus.NOT_FOUND,
        error: 'Not found'
    });
});

db.sequelize.sync().then(() => {
    server.listen(config.port, () => {
        logger.info("[MAIN API SERVER]  Server up. Listen port: " + config.port);
    });
});

module.exports = app;
