const main = process.cwd() + '/';
const logger = require(main + 'utils/logger');
const app = require(main + 'app');
const db = require(main + 'api/models/index.model');
const StarterService = require(main + 'api/services/starter.service');


db.sequelize.sync().then(async () => {
    try {
        await StarterService.startTasks();
    } catch (error) {
        logger.error("[ERROR][StarterComponent] err " + err.message);

    }
});

module.exports = app;
