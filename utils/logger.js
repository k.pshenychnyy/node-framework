/**
 * Configurations of logger.
 */
const main = process.cwd() + '/';
const winston = require('winston');
const winstonRotator = require('winston-daily-rotate-file');
const fs = require('fs');
const dir = './logs';

if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
}

const consoleTransport = new winston.transports.Console({
    'colorize': true
});

const infoTransport = new (winston.transports.DailyRotateFile)({
    name: 'info',
    filename: './logs/info.log',
    datePattern: 'yyyy-MM-dd-',
    prepend: true,
    level: 'info'
});

const errorTransport = new (winston.transports.DailyRotateFile)({
    name: 'error',
    filename: './logs/error.log',
    datePattern: 'yyyy-MM-dd-',
    prepend: true,
    level: 'error'
});

const warnTransport = new (winston.transports.DailyRotateFile)({
    name: 'warn',
    filename: './logs/error.log',
    datePattern: 'yyyy-MM-dd-',
    prepend: true,
    level: 'warn'
});

const logger = winston.createLogger({
    format: winston.format.simple(),
    transports: [
        consoleTransport,
        errorTransport,
        warnTransport,
        infoTransport
    ]
});

module.exports = logger;